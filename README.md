# magento2-shipping-methods-by-customer-group
Magento 2 Module which allows assigning shipping methods to customer groups instead of just having them globally available/unavailable.

## Authors

* Łukasz Owczarczuk
* Jakub Winkler
* Bartosz Górski